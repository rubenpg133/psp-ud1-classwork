# PSP-PRÁCTICA 2: EJERCICIOS DE PROGRAMACIÓN

Escribe en Java los programas necesarios:
- Utiliza de manera adecuada la Programación Orientada a Objetos.
- Presta atención a las excepciones que pueda originar el código e informa al usuario.
- Imprime en pantalla los mensajes necesarios cuando haya que informar al usuario.
- No olvides comentar el código y documentar los métodos.

Enlace al repositorio del codigo fuente: https://gitlab.com/rubenpg133/psp-ud1-classwork

## [Ejercicio1](Ejercicio1/src/es/palacios/Ejercicio1.java) 

Escribe un programa (llámalo Ejercicio1) que reciba como argumentos un comando del sistema
operativo (ejemplo **ls –la**) para ser ejecutado.
- El programa debe crear un proceso hijo que ejecute el comando. 
  - Use el constructor **ProcessBuilder** que toma **List<String>** como parámetro
- El programa también informa con un mensaje si ocurre un error al ejecutar el proceso hijo.
- El proceso padre espera dos segundos a que finalice el proceso hijo. 
  - Imprime un mensaje si el tiempo se agota y termina.
- El padre imprime el valor de salida de la ejecución del proceso hijo. Luego:
  - Si el hijo sale con error, imprime el mensaje de error del hijo y termina.
  - Si el hijo termina normalmente, imprime el resultado de salida de la ejecución del proceso hijo y guarda el resultado en el archivo **output.txt**

Para ejecutarlo hay que hacer Run al Ejercicio1.Java

## [E2-Random10](E2Random10/src/es/palacios/Random10.java)

Crea un programa ejecutable en Java (Ramdom10) que:
- Lea cadenas de su entrada estándar.
- Si la cadena recibida es diferente de **"stop"**, entonces genera un número aleatorio entre 0
- y 10 y lo escribe en su salida estándar.
- El programa finaliza cuando recibe la cadena de **"stop"** de la entrada estándar.

El ejecutable se encuentra en el siguiente enlace: [Ejecutable](out/artifacts/E2Random10_jar/E2Random10.jar)

## [Ejercicio2](Ejercicio2/src/es/palacios/Ejercicio2.java)

Cree un programa ejecutable (Ejercicio2) que:
- Inicia el programa Ramdom10 creado anteriormente como un proceso secundario. 
- Lee líneas de la entrada estándar del usuario. Cada línea leída se envía al subproceso
(utilice los flujos de datos de entrada / salida para comunicar el proceso principal y
secundario) y luego imprime en la pantalla el número aleatorio generado por el proceso
secundario. Escribe todos los números generados aleatoriamente en el archivo:
randoms.txt.
- Termina cuando recibe la cadena "stop".

Para ejecutarlo hay que hacer Run al Ejercicio1.Java

## [E3-Minusculas](E3Minusculas/src/es/palacios/Minusculas.java)

Escriba el programa ejecutable (Minusculas) que:
- Lea líneas de texto de su entrada estándar.
- Transforme cada línea de texto a minúsculas.
- Imprime la línea en minúsculas a su salida estándar.
- Termina cuando recibe la cadena "finalizar".️

El ejecutable se encuentra en el siguiente enlace: [Ejecutable](out/artifacts/E3Minusculas_jar/E3Minusculas.jar)

## [Ejercicio3](Ejercicio3/src/es/palacios/Ejercicio3.java)

Escriba el programa (Ejercicio 3) que:
- Cree el nuevo subproceso **Minusculas**
- Lea las líneas de la entrada estándar y envíe cada una a la entrada estándar del subproceso
**Minusculas**.
- Imprime en la pantalla el texto en minúsculas transformado que el subproceso Minusculas
escribió en su salida estándar.
- Termina cuando recibe la cadena "finalizar".️

Para ejecutarlo hay que hacer Run al Ejercicio1.Java
