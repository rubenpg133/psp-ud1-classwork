package es.palacios;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {

    final static int WAIT_FOR_TIME = 2;

    final static String OUTPUT_FILE_NAME = "output.txt";

    public static void main(String[] args) {
        List<String> command = getCommand(args);
        System.out.println("COMANDO A EJECUTAR: " + command);

        ProcessBuilder pb = new ProcessBuilder(command);
        try {
            Process process = pb.start();
            if (!process.waitFor(WAIT_FOR_TIME, TimeUnit.SECONDS)) {
                System.err.println("El proceso hijo es muy lento Saliendo del programa.");
            } else {
                System.out.println("La ejecución de " + command.toString() + " devuelve el valor de salida: " + process.exitValue());
                if (process.exitValue() == 0) {
                    printAndSaveChildOutput(process);
                } else {
                    System.err.println("ERROR PROCESO HIJO:");
                    showChildError(process);
                    System.exit(4);
                }
            }
        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
            System.exit(2);
        } catch (InterruptedException e1) {
            System.err.println("InterruptedException: " + e1.getMessage());
            System.exit(3);
        }
    }

    /***
     * Este metodo devuelve un comando introducido por los argumentos del programa en formato de una lista de Strings
     * Primero, mira si hay contenido dentro de args y si esta vacio devuelve un mensaje de error diciendo
     * que hace falta un comando para ejecutar.
     * Si args contiene un comando el metodo continuará y inicializa un List de String llamado "command".
     * A continuación, utilizando la clase System crea un String para identificar el sistema operativo en el que
     * se esta ejecutando el programa y si es Windows añade a "command" lo necesario para ejecutar un comando en cmd.
     * Finalmente, añade el contenido de args al List mediante la clase Arrays.
     * @param args es un Array de String que contiene los argumentos del programa.
     * @return devuelve una lista con el comando a ejecutar.
     */
    private static List<String> getCommand(String[] args) {
        if (args.length == 0) {
            System.err.println("Necesito un comando para ejecutar!");
            System.exit(1);
        }

        List<String> command = new ArrayList<>();

        String osName = System.getProperty("os.name") + " Version: " + System.getProperty("os.version");
        if (osName.contains("Windows")) {
            command.add("cmd");
            command.add("/c");
        }

        command.addAll(Arrays.asList(args));
        return command;
    }

    /***
     * Este metodo recibe un proceso y lee el inputStream del mismo que recoge el error producido
     * Luego lo muestra por pantalla
     * @param process es el proceso hijo
     */
    private static void showChildError(Process process) {
        String line;
        try(BufferedReader bf = new BufferedReader(new InputStreamReader(process.getInputStream()));) {
            while ((line = bf.readLine()) != null) {
                System.err.println(line);
            }
        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
        }
    }

    /***
     * Este metodo recibe un proceso y lee el inputStream del mismo que recoge el contenido de la ejecución
     * Luego, lo muestra por pantalla y lo guarda mediante el uso de la clase FileWriter en un fichero.
     * @param process es el proceso hijo
     */
    private static void printAndSaveChildOutput(Process process) {
        String line;
        try(BufferedReader bf = new BufferedReader(new InputStreamReader(process.getInputStream()));
            FileWriter fw = new FileWriter(OUTPUT_FILE_NAME);) {
            while ((line = bf.readLine()) != null) {
                System.out.println(line);
                fw.write(line + System.lineSeparator());
            }
        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
        }
        System.out.println("La ejecución del proceso hijo se ha guardado en el fichero: " + OUTPUT_FILE_NAME);
    }
}
