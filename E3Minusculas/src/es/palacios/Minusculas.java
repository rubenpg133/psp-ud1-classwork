package es.palacios;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Minusculas {
    public static void main(String[] args) throws IOException {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));

        String word;
        boolean goOn = true;
        while (goOn && (word = bufReader.readLine()) != null) {
            if (word.equalsIgnoreCase("finalizar")) {
                goOn = false;
            } else {
                System.out.println(word.toLowerCase());
            }
        }
    }
}
