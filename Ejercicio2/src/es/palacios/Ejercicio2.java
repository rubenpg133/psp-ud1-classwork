package es.palacios;

import java.io.*;

public class Ejercicio2 {

    final static String RANDOM_FILE_NAME = "randoms.txt";

    public static void main(String[] args){
        final String random10JarPath = "out/artifacts/E2Random10_jar/E2Random10.jar";

        Process process = null;
        try {
            process = new ProcessBuilder("java", "-jar", random10JarPath).start();
        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
            System.exit(1);
        }

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(process.getOutputStream());
        InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());

        try (BufferedWriter bufWriter = new BufferedWriter(outputStreamWriter);
             BufferedReader bufReader = new BufferedReader(inputStreamReader);
             FileWriter fw = new FileWriter(RANDOM_FILE_NAME)) {

            BufferedReader bufReadCommLine = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("=========== RANDOM 10 ==========");

            String line;
            boolean goOn;
            do {
                System.out.print("Escribe una cadena para recibir un número del 1 al 10 " +
                        "(Escribe 'Stop' para finalizar el programa): ");
                line = bufReadCommLine.readLine();

                bufWriter.write(line);
                bufWriter.newLine();

                bufWriter.flush();

                goOn = !line.equalsIgnoreCase("stop");
                if (goOn) {
                    String number = bufReader.readLine();
                    System.out.println(number);
                    fw.write(number + System.lineSeparator());
                }
            } while (goOn);

        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
            System.exit(2);
        } finally {
            int exitValue = 0;
            try {
                exitValue = process.waitFor();
            } catch (InterruptedException e) {
                System.err.println("InterruptedException: " + e.getMessage());
                System.exit(3);
            }
            System.out.println(System.lineSeparator() + "La ejecución del programa Random10 devuelve el valor de salida: " + exitValue);
            System.out.println(System.lineSeparator() + "La ejecución del Random10 se ha guardado en el fichero: " + RANDOM_FILE_NAME);
            if (exitValue != 0) {
                System.err.println("ERROR RANDOM10:");
                printChildError(process.getErrorStream());
            }
        }
    }

    /***
     * Este metodo recibe un proceso y lee el errorStream del mismo que recoge el error producido
     * Luego lo muestra por pantalla
     * @param errorStream es el ErrorStream del proceso hijo
     */
    private static void printChildError(InputStream errorStream) {
        BufferedReader br = new BufferedReader(new InputStreamReader(errorStream));
        String line;
        try {
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }

    }
}
