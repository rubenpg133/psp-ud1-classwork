package es.palacios;

import java.io.*;

public class Ejercicio3 {
    public static void main(String[] args) {
        final String minusculasJarPath = "out/artifacts/E3Minusculas_jar/E3Minusculas.jar";

        Process process = null;
        try {
            process = new ProcessBuilder("java", "-jar", minusculasJarPath).start();
        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
            System.exit(1);
        }

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(process.getOutputStream());
        InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());

        try (BufferedWriter bufWriter = new BufferedWriter(outputStreamWriter);
             BufferedReader bufReader = new BufferedReader(inputStreamReader)) {

            BufferedReader bufReadCommLine = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("=========== MINUSCULAS ==========");

            String line;
            boolean goOn;
            do {
                System.out.print("Escribe una cadena en MAYUSCULAS para pasar a minusculas " +
                        "(Escribe 'Finalizar' para detener el programa): ");
                line = bufReadCommLine.readLine();

                bufWriter.write(line);
                bufWriter.newLine();

                bufWriter.flush();

                goOn = !line.equalsIgnoreCase("finalizar");
                if (goOn) {
                    System.out.println(bufReader.readLine());
                }
            } while (goOn);
        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
            System.exit(2);
        } finally {
            int exitValue = 0;
            try {
                exitValue = process.waitFor();
            } catch (InterruptedException e) {
                System.err.println("InterruptedException: " + e.getMessage());
                System.exit(3);
            }
            System.out.println(System.lineSeparator() + "La ejecución del programa Minusculas devuelve el valor de salida: " + exitValue);
            if (exitValue != 0) {
                System.err.println("ERROR MINUSCULAS:");
                printChildError(process.getErrorStream());
            }
        }
    }

    /***
     * Este metodo recibe un proceso y lee el errorStream del mismo que recoge el error producido
     * Luego lo muestra por pantalla
     * @param errorStream es el ErrorStream del proceso hijo
     */
    private static void printChildError(InputStream errorStream) {
        BufferedReader br = new BufferedReader(new InputStreamReader(errorStream));
        String line;
        try {
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
}

