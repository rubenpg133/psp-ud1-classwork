package es.palacios;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Random10 {
    public static void main(String[] args) throws IOException {
        Random random = new Random();
        int number;
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));

        String word;
        boolean goOn = true;
        while (goOn && (word = bufReader.readLine()) != null) {
            if (word.equalsIgnoreCase("stop")) {
                goOn = false;
            } else {
                number = random.nextInt(11);
                System.out.println(number);
            }
        }
    }
}
